// JavaScript File

    // connect to Firebase
    var config = {
        apiKey: "AIzaSyDtzsBxj0FoZtqKaXNM75aCZBPwR5ANGKM",
        authDomain: "cde305-3ade5.firebaseapp.com",
        databaseURL: "https://cde305-3ade5.firebaseio.com",
        projectId: "cde305-3ade5",
        storageBucket: "cde305-3ade5.appspot.com",
        messagingSenderId: "499950705992"
    };
    firebase.initializeApp(config);
    var registeredUsers = [];
    var sortObject = [];
  
    // load the data from firebase
    function load(){
        getDBdata();
    }

// save data to firebase
function saveData(){
    
    var userId = document.getElementById('userid');
    var datetime= new Date();
    userId = datetime.getFullYear()+""+datetime.getMonth()+datetime.getDay()+datetime.getHours()+datetime.getMilliseconds();
    var inputdata = document.querySelectorAll('#formdata input');
    var userProfile = {
        userId: userId,
		username: inputdata[1].value,
		name: inputdata[2].value,
		emailaddress: inputdata[3].value,
		password: inputdata[4].value,
		age: inputdata[5].value
		};
		
    var xhr = new XMLHttpRequest();
    xhr.open('POST','https://cde305-3ade5.firebaseio.com/registeredUsers/.json', true);
    xhr.setRequestHeader('Content-Type','json');
    xhr.send(JSON.stringify(userProfile))
    xhr.onload = function(e) { 
    if(this.status == 200||this.status == 304){
        swal("Successful!","", "success");
        console.log("OK",this.responseText);
        window.location.reload();
    }
    };
    
    getDBdata();
    
}

// show the average age
function showAge() {
    var ages = 0;
    database = firebase.database();
    var ref = database.ref('registeredUsers');
    ref.on('value', gotData, errData);
    
    function gotData(data){
        var values = data.val();
        var keys = Object.keys(values);
        for(var i = 0; i < keys.length; i++){
            var k = keys[i];
            ages += parseInt(values[k].age);
            console.log(ages);
         
        }
        // not user found
    if(ages == 0) {
            //alert("There is not any registered users");
            swal("Warning!","There is not any registered users", "warning");
    } 
    else {
        // user found
        var averageAge = ages / keys.length;
        //alert("the registered users average age is " + averageAge);
        swal("Average Age","The registered users average age is " + averageAge, "info");
    }
        
    }
    function errData(err){
        console.log(err);
    }
    
}

// get the data from firebase and show to table
function getDBdata() {

    var age = [];
    var name = [];
    var username = [];
 
    database = firebase.database();
    var ref = database.ref('registeredUsers');
    ref.on('value', gotData, errData);
    
    function gotData(data){
        
        var values = data.val();
        var keys = Object.keys(values);
        console.log(values);
        //var kk = keys.sort(values[keys].age);
        for(var i = 0; i < keys.length; i++){
            var k = keys[i];
            
            age[i] = (parseInt(values[k].age));
            name[i] = (values[k].name);
            username[i] = (values[k].username);

            
        }
       
        var dbData = "<table class='pure-table pure-table-horizontal'><thead><tr><td>username</td><td onclick='sortByName()'>name</td><td onclick='sortByAge()'>age</td></tr></thead>";
         for(var i = 0; i < keys.length; i++){
             dbData += "<tr><td>" + username[i] + "</td><td>" + name[i] + "</td><td>" + age[i] + "</td></tr>";
             
             
            sortObject.push({'username': username[i], 'name': name[i], 'age': age[i]});
          

    }
    document.getElementById("userInfo").innerHTML = dbData;
    
   
    }
    
   
    function errData(err){
        console.log(err);
    }
}

    // validate the user input
    function validateForm() {
        
      
        var CorrectData = true;
        var x = document.forms["userForm"]["username"].value;
        if (x == "") {
         //alert("User Name must be filled out");
            swal("User name warning!", "User Name must be filled out!", "warning");
            CorrectData = false;
        }

        var username = [];
        database = firebase.database();
        var ref = database.ref('registeredUsers');
        ref.on('value', gotData, errData);
    
        function gotData(data){
        
        var values = data.val();
        var keys = Object.keys(values);
        for(var i = 0; i < keys.length; i++){
            var k = keys[i];
            username[i] = (values[k].username);
        }
        // validate the user name
        for(var k = 0; k < username.length;k++){
            if(username[k] == x){
                //alert("User Name is already created");
                swal("User name warning!", "User Name is already created!", "warning");
                CorrectData = false;
                }
            }
        }
    
   
        function errData(err){
            console.log(err);
        }
    
        // validate the name
        var x = document.forms["userForm"]["name"].value;
        if (x == "") {
            //alert("Name must be filled out");
            swal("Name warning!", "Name must be filled out!", "warning");
            CorrectData = false;
        }
        
        // validate the email
        var email = document.forms["userForm"]["email"].value;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var isEmail = re.test(String(email).toLowerCase());
        if (!isEmail) {
            //alert("email format incorrect");
            swal("Email warning!", "Email format incorrect!", "warning");
            CorrectData = false;
        }
    
        // validate the password
        var x = document.forms["userForm"]["password"].value;
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
        var isStrong = re.test(x);
        if (!isStrong) {
            //alert("Password at least one number, one lowercase and one uppercase letter\nat least six characters");
            swal("Password warning!", "Password at least one number, \none lowercase and one uppercase letter\nat least six characters!", "warning");
            CorrectData = false;
        }
    
        // validate the age
        var a = document.forms["userForm"]["age"].value;
        if (a < 0 || a > 150 || a == "") {
            // alert("Age number must be bigger than 0 and smaller than 150");
            swal("Age warning!", "Age number must be bigger than 0 and smaller than 150!", "warning");
            CorrectData = false;
        }
    
        if(CorrectData){
            saveData();
        }
}
    
    // sort table by name
    function sortByName(){
        sortObject.sort(function(a, b) {
            return ((a.name < b.name) ? -1 : ((a.name == b.name) ? 0 : 1));
        });
        var dbData = "<table class='pure-table pure-table-horizontal'><thead><tr><td>username</td><td onclick='sortByName()'>name</td><td onclick='sortByAge()'>age</td></tr></thead>";
        for(var i = 0; i < sortObject.length; i++){
            dbData += "<tr><td>" + sortObject[i].username + "</td><td>" + sortObject[i].name + "</td><td>" + sortObject[i].age + "</td></tr>";
            
        }
        document.getElementById("userInfo").innerHTML = dbData;
    }
    
    // sort table by age
    function sortByAge(){
        sortObject.sort(function(a, b) {
            return ((a.age < b.age) ? -1 : ((a.age == b.age) ? 0 : 1));
        });
        var dbData = "<table class='pure-table pure-table-horizontal'><thead><tr><td>username</td><td onclick='sortByName()'>name</td><td onclick='sortByAge()'>age</td></tr></thead>";
        for(var i = 0; i < sortObject.length; i++){
            dbData += "<tr><td>" + sortObject[i].username + "</td><td>" + sortObject[i].name + "</td><td>" + sortObject[i].age + "</td></tr>";
            
        }
        document.getElementById("userInfo").innerHTML = dbData;
    }

// filter by age
document.querySelector('#filterByAge input[type="number"]').onkeypress = function() {
    var filterOption = document.getElementById('filterOption').value;
 	var number = parseInt(document.querySelector('#filterByAge input[type="number"]').value);
    var dbData = "<table class='pure-table pure-table-horizontal'><thead><tr><td>username</td><td onclick='sortByName()'>name</td><td onclick='sortByAge()'>age</td></tr></thead>";
    for(var i = 0; i < sortObject.length; i++){
        if(filterOption == ">="){
            if(parseInt(sortObject[i].age) >= number){
            dbData += "<tr><td>" + sortObject[i].username + "</td><td>" + sortObject[i].name + "</td><td>" + sortObject[i].age + "</td></tr>";
            }
        }else{
            if(filterOption == "<="){
                if(parseInt(sortObject[i].age) <= number){
                dbData += "<tr><td>" + sortObject[i].username + "</td><td>" + sortObject[i].name + "</td><td>" + sortObject[i].age + "</td></tr>";
                }
            }   
        }
    }
    document.getElementById("userInfo").innerHTML = dbData;
};



  