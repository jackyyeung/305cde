
// use the user input to get the city info from open weather and save to Firebase
exports.postFB = function(db,postjsondata,name){
    var timeInMss = new Date();
    var dd = timeInMss.getDate();
    var mm = timeInMss.getMonth()+1;
    var yyyy = timeInMss.getFullYear();
    var da = yyyy + "-" + mm + "-" + dd;
    var ref = db.ref('/city/'+name);
    var value = {
        Name: name,
        Temp: postjsondata.main.temp,
         Maxtemp: postjsondata.main.temp_max,
        Mintemp: postjsondata.main.temp_min,
        Humidity: postjsondata.main.humidity,
        Wind: postjsondata.wind.speed,
        Weather: postjsondata.weather[0].description+'',
        UpdateDate: da
    }
    ref.set(value);
    return ""
}

// use the user input to get the restaurants info from Google Place and save to Firebase
exports.restaurantsFB = function(db,postjsondata,name){
    for(var i = 0; i < 5; i++){
    var ref = db.ref('/city/'+name+'/restaurants/'+postjsondata.results[i].name);
    var value = {
        Name: postjsondata.results[i].name,
        Location: postjsondata.results[i].vicinity,
        Rating: postjsondata.results[i].rating
       
        
    }
    ref.set(value);
    }
    return ""
}