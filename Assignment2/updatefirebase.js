
// use the user input to update the city info from Firebase
exports.putFB = function(db,updatejsondata,name){
    var ref = db.ref('/city/'+name);
    var timeInMss = new Date();
    var dd = timeInMss.getDate();
    var mm = timeInMss.getMonth()+1;
    var yyyy = timeInMss.getFullYear();
    var da = yyyy + "-" + mm + "-" + dd;
    var value = {
        Temp: updatejsondata.main.temp,
         Maxtemp: updatejsondata.main.temp_max,
        Mintemp: updatejsondata.main.temp_min,
        Name: name,
        Humidity: updatejsondata.main.humidity,
        Wind: updatejsondata.wind.speed,
        Weather: updatejsondata.weather[0].description+'',
        UpdateDate: da
    }
    ref.update(value);
    return ""
}

// use the user input to uppdate the restaurants info from Google Place and save to Firebase
exports.putrestaurantsFB = function(db,postjsondata,name){
    for(var i = 0; i < 5; i++){
    var ref = db.ref('/city/'+name+'/restaurants/'+postjsondata.results[i].name);
    var value = {
        Name: postjsondata.results[i].name,
        Location: postjsondata.results[i].vicinity,
        Rating: postjsondata.results[i].rating
       
        
    }
    ref.set(value);
    }
    return ""
}