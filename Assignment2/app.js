var restify = require('restify');
var request = require('request');
const url = require("./api.js")
const postfirebaseurl = require("./postfirebase.js")
const updatefirebaseurl = require("./updatefirebase.js")
const deletefirebaseurl = require("./delfirebase.js")
const getfirebaseurl = require("./getfirebase.js")

var express        = require( 'express' );
var http           = require( 'http' );

var app            = express();
app.set( 'port', process.env.PORT || 3001 );
app.get('/', function (req, res) {
    res.send('Hello World');
});

http.createServer( app ).listen( app.get( 'port' ), function (){
  console.log( 'Express server listening on port ' + app.get( 'port' ));
});

var firebase = require("firebase");
var config = {
    apiKey: "AIzaSyDtzsBxj0FoZtqKaXNM75aCZBPwR5ANGKM",
    authDomain: "cde305-3ade5.firebaseapp.com",
    databaseURL: "https://cde305-3ade5.firebaseio.com",
    projectId: "cde305-3ade5",
    storageBucket: "cde305-3ade5.appspot.com",
    messagingSenderId: "499950705992"
  };
  firebase.initializeApp(config);
    var db = firebase.database();
    
var name = null;
var postjsondatapostjsondata = null;
var updatejsondata = null;
var postjsondata = null;
var citylat = null;
var citylon = null;
var updateRjsondata = null;


const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
 
app.use(restify.plugins.acceptParser(server.acceptable));
app.use(restify.plugins.queryParser());
app.use(restify.plugins.bodyParser());


// post method API by city name
app.post('/:name', function(req,res){
  var city = JSON.stringify(req.params)
  name = JSON.parse(city)
  var apiURL = url.citynameapi(name.name)
  request.get(apiURL, (err,res2,body) =>{
    postjsondata = JSON.parse(body);
    postfirebaseurl.postFB(db,postjsondata,name.name);
    citylat = postjsondata.coord.lat
    citylon = postjsondata.coord.lon
    var restaurantURL = url.restaurantapi(citylat,citylon)
    request.get(restaurantURL, (err,res3,body) =>{
     postjsondata = JSON.parse(body);
     res.send({message : "successful"});
     res.end();
     postfirebaseurl.restaurantsFB(db,postjsondata,name.name);
   
  });
  });
    
});
 
 
// post method API by lat and lon 
app.post('/:lat/:lon', function(req,res){
  var latlonJson = JSON.stringify(req.params);
  
  var latlon = JSON.parse(latlonJson);
  var apiURL = url.latlonapi(latlon.lat,latlon.lon)
  console.log(latlon.lat);
  request.get(apiURL, (err,res2,body) =>{
    var jsonFromlatlon = JSON.parse(body);
    postfirebaseurl.postFB(db,jsonFromlatlon,jsonFromlatlon.name);
    var restaurantURL = url.restaurantapi(latlon.lat,latlon.lon)
    request.get(restaurantURL, (err,res3,body) =>{
     postjsondata = JSON.parse(body);
     res.send({message : "successful"});
     res.end();
     postfirebaseurl.restaurantsFB(db,postjsondata,jsonFromlatlon.name);
   
  });
  });
  
});


// update method API by city name
app.put('/:cityname',function(req, res) {
    var isNew = true;
    var city = JSON.stringify(req.params);
    var name = JSON.parse(city);
    var dbdata = new Array();
    var ref = db.ref('/city/');
    ref.once('value').then(function(snapshot) {
       
         snapshot.forEach(function(data){
             dbdata.push(data.val().Name);
             
         });
          for(var i =0; i<dbdata.length;i++){
              if(name.cityname == dbdata[i]){
                  isNew = true;
                  break
              }else{
                  isNew = false;
              }
          }
          console.log(isNew);
    if(isNew == true){
               
        var apiURL = url.citynameapi(name.cityname);
 
        request.get(apiURL, (err,res2,body) =>{
            var updatejsondata = JSON.parse(body);
            updatefirebaseurl.putFB(db,updatejsondata,name.cityname);
            citylat = updatejsondata.coord.lat
            citylon = updatejsondata.coord.lon
       
            var restaurantURL = url.restaurantapi(citylat,citylon)
            request.get(restaurantURL, (err,res3,body) =>{
                updateRjsondata = JSON.parse(body);
                res.send({message : "successful"});
                res.end();
                updatefirebaseurl.putrestaurantsFB(db,updateRjsondata,updatejsondata.name);
   
            });
        });
    }else{
      res.end();
    }
    });
   
  
});



// delete city information API by city name
app.del('/:cityname',function(req, res) {
    var city = JSON.stringify(req.params)
  name = JSON.parse(city)
  deletefirebaseurl.delFB(db,name.cityname);
  res.send({message : "successful"});
  res.end();
});

// get all data API
app.get('/all',function(req, res) {
    var ref = db.ref();
    ref.once('value').then(function(snapshot) {
         res.send(snapshot.val());
         res.end();
});
});


// get city information API by city name
app.get('/:cityname',function(req, res) {
  var city = JSON.stringify(req.params)
  name = JSON.parse(city)
  getfirebaseurl.getFB(db,name.cityname,res);
  
});

app.get('/threeday/:threeday', function(req, res) {
    var cityname = JSON.stringify(req.params)
    var name = JSON.parse(cityname)
    var city = name.threeday
    
    console.log(name)
    request.get("https://api.openweathermap.org/data/2.5/forecast?q="+city+"&appid=a3c3a732fc257195224df3f2984d4d9e&units=metric", (err,res2,body) =>{
    res.send(body);
    res.end();
  });
});


app.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});