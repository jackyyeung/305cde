// use the city name to get the city info from open weather api
exports.citynameapi = function(cityname){
    return 'http://api.openweathermap.org/data/2.5/weather?q='+ cityname+'&appid=a3c3a732fc257195224df3f2984d4d9e&units=metric'
}


// use the city lat and lon to get the city info from open water api
exports.latlonapi = function(lat, lon){
    return "http://api.openweathermap.org/data/2.5/weather?lat="+lat +"&lon="+lon+"&appid=a3c3a732fc257195224df3f2984d4d9e&units=metric"
}

// search some restaurants where are user input the city
exports.restaurantapi = function(lat, lon){
    return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+lon+"&radius=500&type=restaurant&key=AIzaSyCqchQ1Q97H1cGApx_WayatquCHyjEoxuU"
}