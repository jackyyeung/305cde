var sortObject = [];




function load() {
    getallDBdata();
}


function getallDBdata() {
    
    var requesta = new XMLHttpRequest();

    requesta.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/city', true);
    requesta.onload = function() {
        var dbData = '<table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Name</td><td>Temp</td><td>Mintemp</td><td>Maxtemp</td><td>Humidity</td><td>Description</td><td>Wind</td><td>LastUpdate</td><td>Add Favourites</td></tr></thead>';
        
        var j = JSON.parse(this.responseText);
        for(var i in j){
            for(var i2 in j[i]){
               
                if(j[i][i2].Name != null){
                    dbData += '<tr><td>' + j[i][i2].Name + '</td><td>' + j[i][i2].Temp + ' °C</td><td>' + j[i][i2].Mintemp + ' °C</td><td>' + j[i][i2].Maxtemp + ' °C</td><td>' + j[i][i2].Humidity + '%</td><td>' + j[i][i2].Weather + '</td><td>' + j[i][i2].Wind + '</td><td>' + j[i][i2].UpdateDate + '</td><td><a href="/addBookMark/'+j[i][i2].Name+'">Add</a></td></tr>';
                   
                }
            }
       
        }
        
        document.getElementById("allInfo").innerHTML = dbData;
        
    }
    requesta.send();

  
}


function searchcity(){
    var cityname = document.forms["searchform"]["cityname"].value;
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/get/'+cityname, true);
    request.onload = function() {
        var resData = '<h1 class="font">Some Restaurants Information</h1><table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Name</td><td>Rating</td><td>Location</td></tr></thead>';
        var dbData = '<h1 class="font">Search Result</h1><table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Temp</td><td>Mintemp</td><td>Maxtemp</td><td>Humidity</td><td>Description</td><td>Wind</td><td>LastUpdate</td><td>Add Favourites</td></tr></thead>';
        var j = JSON.parse(this.responseText);
        dbData += '<tr><td>' + j.Temp + ' °C</td><td>' + j.Mintemp + ' °C</td><td>' + j.Maxtemp + ' °C</td><td>' + j.Humidity + '%</td><td>' + j.Weather + '</td><td>' + j.Wind + '</td><td>' + j.UpdateDate + '</td><td><a href="/addBookMark/'+cityname+'">Add</a></td></tr>';
        for(var i in j.restaurants){
             resData += "<tr><td>" + j.restaurants[i].Name + "</td><td>" + j.restaurants[i].Rating + "</td><td>" + j.restaurants[i].Location + "</td></tr>";
        }
       
        
        document.getElementById("cityInfo").innerHTML = dbData;
        document.getElementById("restaurantsInfo").innerHTML = resData;
    }
    request.send();
    getForecastdata();
}

function getForecastdata() {
    var cityname = document.forms["searchform"]["cityname"].value;
    var request2 = new XMLHttpRequest();

    request2.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/getthreeday/'+cityname, true);
    request2.onload = function() {
        var dbData2 = '<h1 class="font">Forecast Weather</h1><table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Date</td><td>Temp</td><td>Mintemp</td><td>Maxtemp</td><td>Humidity</td><td>Description</td></tr></thead>';
        var j = JSON.parse(this.responseText);
        for (var i = 0; i < j.list.length; i = i + 8) {
            dbData2 += "<tr><td>" + j.list[i].dt_txt + "</td><td>" + j.list[i].main.temp + " °C</td><td>" + j.list[i].main.temp_min + " °C</td><td>" + j.list[i].main.temp_max + " °C</td><td>" + j.list[i].main.humidity + "%</td><td>" + j.list[i].weather[0].description + "</td></tr>";
            
        }
        document.getElementById("forecastInfo").innerHTML = dbData2;
    }
    request2.send();
}

function addcity(){
    var cityname = document.forms["addform"]["cityname"].value;
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/post/'+cityname, true);
    request.onload = function() {
       getallDBdata();
      
    }
    request.send();
    
}

function updatecity(){
    var cityname = document.forms["updateform"]["cityname"].value;
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/update/'+cityname, true);
    request.onload = function() {
       getallDBdata();
    }
    request.send();
    
}

function deletecity(){
    var cityname = document.forms["deleteform"]["cityname"].value;
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/delete/'+cityname, true);
    request.onload = function() {
       getallDBdata();
    }
    request.send();
    
}

function logout(){
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/logout', true);
    request.onload = function() {
       window.location.href = 'https://vtc305cde-jackyyeung-1.c9users.io/';
    }
    request.send();
    
}

function bookMark(){
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/favourite', true);
    request.onload = function() {
     window.location.href = 'https://vtc305cde-jackyyeung-1.c9users.io/favourite';
    }
    request.send();
    
}

