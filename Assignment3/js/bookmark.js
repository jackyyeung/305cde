
function load() {
    getAllBM();
}

function getAllBM(){
    var requesta = new XMLHttpRequest();

    requesta.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/favourites', true);
    requesta.onload = function() {
        //var dbData = '<table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Name</td><td>Temp</td><td>Mintemp</td><td>Maxtemp</td><td>Humidity</td><td>Description</td><td>Wind</td><td>LastUpdate</td><td>Add Favourites</td></tr></thead>';
        
        var j = this.responseText;
        document.getElementById("allInfo").innerHTML = j;
        
    }
    requesta.send();
}


function searchcity(){
    var cityname = document.forms["searchform"]["cityname"].value;
    var request = new XMLHttpRequest();
    request.open('GET', 'https://vtc305cde-jackyyeung-1.c9users.io/get/'+cityname, true);
    request.onload = function() {
        var resData = '<h1 class="font">Some Restaurants Information</h1><table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Name</td><td>Rating</td><td>Location</td></tr></thead>';
        var dbData = '<h1 class="font">Search Result</h1><table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>Temp</td><td>Mintemp</td><td>Maxtemp</td><td>Humidity</td><td>Description</td><td>Wind</td><td>LastUpdate</td><td>Add Favourites</td></tr></thead>';
        var j = JSON.parse(this.responseText);
        dbData += '<tr><td>' + j.Temp + ' °C</td><td>' + j.Mintemp + ' °C</td><td>' + j.Maxtemp + ' °C</td><td>' + j.Humidity + '%</td><td>' + j.Weather + '</td><td>' + j.Wind + '</td><td>' + j.UpdateDate + '</td><td><a href="/addBookMark/'+cityname+'">Add</a></td></tr>';
        for(var i in j.restaurants){
             resData += "<tr><td>" + j.restaurants[i].Name + "</td><td>" + j.restaurants[i].Rating + "</td><td>" + j.restaurants[i].Location + "</td></tr>";
        }
       
        
        document.getElementById("cityInfo").innerHTML = dbData;
        document.getElementById("restaurantsInfo").innerHTML = resData;
    }
    request.send();
    getForecastdata();
}