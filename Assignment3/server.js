var express = require('express');

var app = express();
var http = require('http').Server(app);
var port = process.env.POST || 8080;
var session = require('express-session');
var request = require('request');
var bodyParser = require('body-parser');
var url = require("./connectfirebase.js");
var XMLHttpRequest = require('xhr2');
var xhr = new XMLHttpRequest();
var database = url.firedb();
var ref = database.ref('registeredUsers');

// var firebase = require('firebase')
// var config = {
//     apiKey: "AIzaSyDtzsBxj0FoZtqKaXNM75aCZBPwR5ANGKM",
//     authDomain: "cde305-3ade5.firebaseapp.com",
//     databaseURL: "https://cde305-3ade5.firebaseio.com",
//     projectId: "cde305-3ade5",
//     storageBucket: "cde305-3ade5.appspot.com",
//     messagingSenderId: "499950705992"
// };
var sortObject = [];
// firebase.initializeApp(config);
// var db = firebase.database();

app.use(express.urlencoded({
    extended: true
}));


app.use(session({
    secret: 'abc',
    resave: true,
    saveUninitialized: true
}));

app.use(express.static(__dirname + '/fonts'));
app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/vendor'));
app.use(express.static(__dirname + '/images'));
app.use(express.static(__dirname + '/js'));
app.set('views', __dirname + '/');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


app.get('/', function(req, res) {
    if (req.session.susername != null) {
        res.set('Content-Type', 'text/html');
        // res.send(dbData);
        res.render("home.html", { message: "Welcome! " + req.session.susername });
        res.end();
    }
    else {
        res.set('Content-Type', 'text/html');
        res.render("login.html", { message: "" });
        res.end();
    }
});


app.post('/signup', function(req, res) {

    res.set('Content-Type', 'text/html');
    res.render("signup.html", { message: "" });
    res.end();

});

app.get('/favourite', function(req, res) {

    res.set('Content-Type', 'text/html');
    res.render("favourite.html", { message: "Favourite List" });
    res.end();

});

app.post('/saveUser', function(req, res) {
    var username = req.body.username;
    var password = req.body.pass;
    var name = req.body.name;
    var email = req.body.email;
    var age = req.body.age;

    var userProfile = {
        username: username,
        password: password,
        name: name,
        email: email,
        age: age
    };
    var ref = database.ref("/registeredUsers/" + username);
    ref.once("value", function(snapshot) {
        ref.set(userProfile);
        res.set('Content-Type', 'text/html');
        res.render("login.html", { message: "Welcome! " + req.session.susername });
        res.end();
    });

});

app.get('/addBookMark/:citybm', function(req, res) {
    console.log("addBMMMM       " + req.session.susername);
    var cityname = JSON.stringify(req.params);
    var name = JSON.parse(cityname);
    var city = name.citybm;
    var user = req.session.susername;
    var userProfile = {
        BookMark: city
    };
    if (user != null) {
        var ref = database.ref("/bookMark/" + user + "/" + city);
        ref.once("value", function(snapshot) {
            ref.set(userProfile);
            res.set('Content-Type', 'text/html');
            res.render("home.html", { message: "" });
            res.end();
        });



    }
});


app.get('/get/:city', function(req, res) {
    var cityname = JSON.stringify(req.params);
    var name = JSON.parse(cityname);
    var city = name.city;
    xhr = new XMLHttpRequest();
    var url = "https://weatherinfoassignment.herokuapp.com/" + city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            res.send(this.responseText);
            res.end();
        }
    };
    xhr.open("GET", url, true);
    xhr.send();


});

app.get('/favourites', function(req, res) {
    var dbData = '<table class="pure-table pure-table-horizontal" style="font-weight: bold;"><thead><tr><td>City Name</td><td>Detail</td><td>Note</td><td>Delete</td></tr></thead>';
    var ref2 = database.ref('bookMark/' + req.session.susername);
    ref2.on('value', gotData, errData);



    function gotData(data) {

        var values = data.val();
        var keys = Object.keys(values);
        console.log(keys.length);
        for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            console.log(values[k].BookMark);
            dbData += '<tr><td>' + values[k].BookMark + '</td><td><Button type="button" class="button">Detail</Button></td><td><Button type="button" class="button">Note</Button></td><td><Button type="button" class="button">Delete</Button</td></tr>';
           console.log(dbData);


        }
       
         res.send(dbData)
        res.end();
       

    }

    function errData(err) {
        console.log(err);
    }


});


app.get('/city', function(req, res) {
    xhr = new XMLHttpRequest();
    var url = "https://weatherinfoassignment.herokuapp.com/all";

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            res.send(this.responseText)
            res.end()
        }
    };
    xhr.open("GET", url, true);
    xhr.send();


});

app.get('/logout', function(req, res) {
    req.session.susername = null;
    console.log("logout")
    res.set('Content-Type', 'text/html');
    res.render("login.html", { message: "" });
    res.end();


});

app.get('/getthreeday/:city', function(req, res) {
    var cityname = JSON.stringify(req.params);
    var name = JSON.parse(cityname);
    var city = name.city;
    xhr = new XMLHttpRequest();
    var url = "https://weatherinfoassignment.herokuapp.com/threeday/" + city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //var myArr = JSON.parse(this.responseText);

            res.send(this.responseText)
            res.end()
        }
    };
    xhr.open("GET", url, true);
    xhr.send();


});

app.get('/post/:city', function(req, res) {
    var cityname = JSON.stringify(req.params);
    var name = JSON.parse(cityname);
    var city = name.city;
    xhr = new XMLHttpRequest();
    var url = "https://weatherinfoassignment.herokuapp.com/" + city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            res.end();
        }
    };
    xhr.open("POST", url, true);
    xhr.send();


});

app.get('/update/:city', function(req, res) {
    var cityname = JSON.stringify(req.params);
    var name = JSON.parse(cityname);
    var city = name.city;
    xhr = new XMLHttpRequest();
    var url = "https://weatherinfoassignment.herokuapp.com/" + city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(this.responseText);
            res.render("home.html", { message: "ok" });
            res.end();
        }
    };
    xhr.open("PUT", url, true);
    xhr.send();


});

app.get('/delete/:city', function(req, res) {
    var cityname = JSON.stringify(req.params);
    var name = JSON.parse(cityname);
    var city = name.city;
    xhr = new XMLHttpRequest();
    var url = "https://weatherinfoassignment.herokuapp.com/" + city;

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(this.responseText);
            res.render("home.html", { message: "ok" });
            res.end();
        }
    };
    xhr.open("DELETE", url, true);
    xhr.send();


});





app.post('/login', function(req, res) {
    var age = "";
    var name = "";
    ``
    var username = "";
    var isUser = "false"
    var signin_username = req.body.username;
    var signin_password = req.body.pass;
    if (signin_username != null) {
        ref.on('value', gotData, errData);

        console.log(signin_username);
        var dbaccount = new Array();

        function gotData(data) {

            var values = data.val();
            var keys = Object.keys(values);

            for (var i = 0; i < keys.length;) {
                var k = keys[i];
                console.log(keys.length + " " + values[k].password + " " + signin_password + "    " + signin_username);
                i = i + 1;
                if (values[k].username == signin_username && values[k].password == signin_password) {

                    age = (parseInt(values[k].age));
                    name = (values[k].name);
                    username = (values[k].username);
                    var dbData = "<table><tr><td>username</td><td>name</td><td>age</td></tr>";
                    dbData += "<tr><td>" + username + "</td><td>" + name + "</td><td>" + age + "</td></tr>";
                    isUser = "true"
                    req.session.susername = signin_username;
                    req.session.spassword = signin_password;
                    // res.session.destroy();
                    res.set('Content-Type', 'text/html');
                    // res.send(dbData);
                    res.render("home.html", { message: "Welcome! " + req.session.susername });
                    res.end();
                    break;
                }
                else if (i == keys.length) {
                    res.set('Content-Type', 'text/html');
                    res.render("login.html", { message: "username or password is no correct" });
                    res.end();
                }

            }





        }


        function errData(err) {
            console.log(err);
        }
    }
});


http.listen(port, function() {
    console.log('Server Port : ' + port);
});
